/**
 * SPDX-FileCopyrightText: (c) 2020 Carl Schwan <carl@carlschwan.eu>
 * SPDX-FileCopyrightText: 2022 Nguyen Hung Phu <phu.nguyen@kdemail.net>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

document.querySelectorAll('.swiper').forEach((swiperElement) => {
  console.log('centerd', swiperElement.dataset.centered ?? true)
  let swiper = new Swiper(swiperElement, {
    centeredSlides: swiperElement.dataset.centered ?? true,
    slidesPerView: 'auto',
    spaceBetween: 30,
    loop: swiperElement.dataset.centered ?? false,
  
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });
})
