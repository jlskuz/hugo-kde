/**
 * SPDX-FileCopyrightText: (c) 2020 Carl Schwan <carl@carlschwan.eu>
 * SPDX-FileCopyrightText: 2022 Phu Hung Nguyen <phu.nguyen@kdemail.net>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

document.querySelectorAll('.preview').forEach(preview => {
    preview.addEventListener('click', () => {
        const imagepreview = document.querySelector('#kImagePreview img');
        imagepreview.setAttribute('src', preview.getAttribute('src'));
        imagepreview.setAttribute('alt', preview.getAttribute('alt'));
        $("#kImagePreview").modal('show');
    });
});

// add autoplay for tabs video
document.querySelectorAll('.tab-pane.video').forEach(function(tab) {
    tab = tab.id;
    const nav = document.getElementById(`${tab}-control`);
    const video = document.querySelector(`#${tab} video`);
    const button = document.querySelector(`#${tab} button`);

    nav.addEventListener('click', function() {
        if (!button.classList.contains('d-none')) {
            button.classList.add('d-none');
        }
        video.currentTime = 0;
        video.play();
    });

    video.addEventListener('ended', function() {
        if (button.classList.contains('d-none')) {
            button.classList.remove('d-none');
        }
    });

    button.addEventListener('click', function() {
        button.classList.add('d-none');
        video.currentTime = 0;
        video.play();
    });
});

// add autoplay for tabs with two videos (laptop and mobile)
document.querySelectorAll('.tab-pane.two-videos').forEach(function(tab) {
    tab = tab.id;
    const nav = document.getElementById(`${tab}-control`);
    const video_mobile = document.querySelector(`#${tab} .phone-overlay video`);
    const video_laptop = document.querySelector(`#${tab} .laptop-overlay video`);
    const button = document.querySelector(`#${tab} button`);

    nav.addEventListener('click', function() {
        if (!button.classList.contains('d-none')) {
            button.classList.add('d-none');
        }
        video_mobile.currentTime = 0;
        video_laptop.currentTime = 0;
        video_mobile.play();
        video_laptop.play();
    });

    video_mobile.addEventListener('ended', function() {
        if (button.classList.contains('d-none')) {
            button.classList.remove('d-none');
        }
    });

    button.addEventListener('click', function() {
        button.classList.add('d-none');
        video_mobile.currentTime = 0;
        video_laptop.currentTime = 0;
        video_mobile.play();
        video_laptop.play();
    });
});

// create config object: rootMargin and threshold
// are two properties exposed by the interface
const config = {
  rootMargin: '0px 0px 50px 0px',
  threshold: 0
};

// register the config object with an instance
// of intersectionObserver
let observer = new IntersectionObserver(function(entries, self) {
    // iterate over each entry
    entries.forEach(entry => {
        // process just the images that are intersecting.
        // isIntersecting is a property exposed by the interface
        if(entry.isIntersecting) {
            // custom function that copies the path to the img
            // from data-src to src
            entry.target.src = entry.target.dataset.src;
            // the image is now in place, stop watching
            self.unobserve(entry.target);
        }
    });
}, config);


const imgs = document.querySelectorAll('[data-src]');
imgs.forEach(img => {
    observer.observe(img);
});

document.querySelectorAll('.nav-features .nav-link').forEach(nav => {
    nav.addEventListener('mouseover', function(event) {
        const tab_content = document.querySelector(`${event.target.getAttribute("href")} [data-src]`);
        if (tab_content) {
            tab_content.src = tab_content.dataset.src;
        }
    });
});

document.querySelectorAll(".dropdown-trans .dropdown-item").forEach(menu => {
    menu.addEventListener("click", (event) => {
        localStorage = window.localStorage;
        localStorage.setItem('lang', event.target.lang);
    })
});

function convertLangCode(lang) {
    if (lang == 'pt') {
        return 'pt-pt';
    } else if (lang == 'no') {
        return 'nn';  // though nb seems to be used by more Norwegians, more websites are translated to nn
    } else if (lang == 'zh') {
        return 'zh-cn';
    } else if (lang.startsWith('de')) {
        return 'de';
    } else if (lang.startsWith('en') && !lang.toLowerCase().endsWith('gb')) {
        return 'en';
    } else if (lang.startsWith('es')) {
        return 'es';
    } else if (lang.startsWith('fr')) {
        return 'fr';
    } else if (lang.startsWith('it')) {
        return 'it';
    } else if (lang.startsWith('tr')) {
        return 'tr';
    } else if (lang.startsWith('vi')) {
        return 'vi';
    } else {
        hugoLang = lang.toLowerCase().replace(/[@_]/, '-');
        parts = hugoLang.split('-')
        if (parts.length > 1) {
            // the next 4 lines are just translated from the Python function and very likely to be useless here
            if (parts[1] == 'ijekavian') {
                hugoLang = `${parts[0]}-ije`
            } else if (parts[1] == 'ijekavianlatin') {
                hugoLang = `${parts[0]}-il`
            } else if (parts[1].length > 2) {
                hugoLang = `${parts[0]}-${parts[1].slice(0, 2)}`
            }
        }
        return hugoLang;
    }
}

if (window.location.hostname != 'planet.kde.org') {
    localStorage = window.localStorage;
    const lang = localStorage.getItem("lang") || convertLangCode(navigator.language);
    console.log(`${navigator.language} - ${lang}`);
    if (lang) {
        const currentLang = document.querySelector("html").lang;
        const hasTranslation = document.querySelector('link[rel="alternate"][hreflang="' + lang + '"]');
        if (currentLang !== lang && currentLang === 'en' && hasTranslation) {
            // We moved to an english page while having previously choosen another language
            window.location.href = hasTranslation.href + window.location.search + window.location.hash;
        }
    }
}
