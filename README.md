# hugo-kde
This is the Hugo theme shared among several KDE websites.

## Usage

You need recent versions of Hugo and Go installed. Then clone

```bash
git clone git@invent.kde.org/websites/hugo-kde
```
Read more in the [wiki](https://invent.kde.org/websites/hugo-kde/-/wikis/).

## Code style

New code should be compatible with these conventions:
- **Indentation**: 2 spaces
- One line between each CSS declaration
- One SASS module for each logical component.
